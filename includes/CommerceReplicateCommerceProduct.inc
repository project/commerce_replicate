<?php

/**
 * Class CommerceReplicateCommerceProduct.
 */
class CommerceReplicateCommerceProduct {

  /**
   * Replicate a commerce product reference.
   *
   * @param object $entity
   *   The commerce_product entity to modify.
   * @param string $field_name
   *   The name of the field.
   * @param string $language
   *   The language of the field value.
   */
  public static function cloneEntity($entity, $field_name, $language) {
    if (!isset($entity->{$field_name}[$language]) || count($entity->{$field_name}[$language]) === 0) {
      return;
    }
    $new_field = array();
    foreach ($entity->{$field_name}[$language] as $old_field) {
      $loaded_product = commerce_product_load($old_field['product_id']);
      $new_product = replicate_clone_entity('commerce_product', $loaded_product);
      commerce_product_save($new_product);
      $new_field[] = array(
        'product_id' => $new_product->product_id,
      );
    }
    $entity->{$field_name}[$language] = $new_field;
  }

}
