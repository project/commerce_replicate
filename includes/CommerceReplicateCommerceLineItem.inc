<?php

class CommerceReplicateCommerceLineItem {
  /**
   * Replicate a commerce line item reference.
   *
   * @param object $entity
   *   The commerce_line_item entity to modify.
   * @param string $field_name
   *   Name of the field.
   * @param string $language
   *   Language of the field.
   */
  public static function cloneEntity($entity, $field_name, $language) {
    if (!isset($entity->{$field_name}[$language]) || count($entity->{$field_name}[$language]) === 0) {
      return;
    }
    $new_field = array();
    foreach ($entity->{$field_name}[$language] as $old_field) {
      $loaded_line_item = commerce_line_item_load($old_field['line_item_id']);
      $new_line_item = replicate_clone_entity('commerce_line_item', $loaded_line_item);
      commerce_line_item_save($new_line_item);
      $new_field[] = array(
        'line_item_id' => $new_line_item->line_item_id,
      );
    }
    $entity->{$field_name}[$language] = $new_field;
  }
}