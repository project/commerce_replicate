<?php

class CommerceReplicateCommerceCustomerProfile {
  /**
   * Replicate a commerce line item reference.
   *
   * @param object $entity
   *   The commerce_line_item entity to modify.
   * @param string $field_name
   *   Name of the field.
   * @param string $language
   *   Language of the field.
   */
  public static function cloneEntity($entity, $field_name, $language) {
    if (!isset($entity->{$field_name}[$language]) || count($entity->{$field_name}[$language]) === 0) {
      return;
    }
    $new_field = array();
    foreach ($entity->{$field_name}[$language] as $old_field) {
      $loaded_customer_profile = commerce_customer_profile_load($old_field['profile_id']);
      $new_customer_profile = replicate_clone_entity('commerce_customer_profile', $loaded_customer_profile);
      commerce_customer_profile_save($new_customer_profile);
      $new_field[] = array(
        'profile_id' => $new_customer_profile->profile_id,
      );
    }
    $entity->{$field_name}[$language] = $new_field;
  }
}