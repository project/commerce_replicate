# Introduction
The [Commerce Replicate](https://www.drupal.org/project/commerce_replicate) module contains code to combine [Drupal Commerce](https://www.drupal.org/project/commerce) with the [Replicate](https://www.drupal.org/project/replicate) module. If required, it will also play well with the [Replicate UI](https://www.drupal.org/project/replicate_ui) module by installing the *Commerce Replicate UI* submodule.

# Configuration
As with *Replicate UI* module, there is nothing to configure really. If a user has access rights to view an order and create a new one, he will be able to replicate it too.

# What does it really do?
Based on the functionality provided by the *Replicate* module, this module provides adds support for replicating `commerce_product`, `commerce_line_items`, `commerce_customer_profile` and `commerce_order` entities. Line Items and Customer Profiles get automatically cloned when cloning an entity, whereas references to commerce_product entities don't get handled. 

# FAQ
##### Q: What if I have to further modify the entities supported by this module?
Use the same hook implementations used by this module in your own custom module. I would specifically suggest to look for `hook_replicate_entity_ENTITY_TYPE()` which is introduced by the *Replicate* module.

##### Q: I really find it important to clone the commerce_product entities too when referenced.
Just use the following code in a custom module:
```
/**
 * Implements hook_replicate_field_FIELD_TYPE().
 */
function MYMODULE_replicate_field_commerce_product_reference(&$entity, $entity_type, $field_name) {
  foreach ($entity->$field_name as $language => $values) {
    CommerceReplicateCommerceProduct::cloneEntity($entity, $field_name, $language);
  }
}
```