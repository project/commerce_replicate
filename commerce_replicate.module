<?php

/**
 * @file
 * The main module file.
 */

/**
 * Implements hook_replicate_entity_ENTITY_TYPE().
 *
 * Commerce Product entities don't need a corresponding
 * hook_replicate_field_FIELD_TYPE() implementation only because it feels wrong.
 * For example, when you clone a commerce_order entity, you have to clone its
 * commerce_customer_profiles but the commerce_line_item entities should point
 * to the same commerce_product entities with the original order.
 */
function commerce_replicate_replicate_entity_commerce_product($entity) {
  $entity->revision_id = NULL;
  $entity->revision_timestamp = NULL;
  $entity->product_id = NULL;
  $entity->sku = $entity->sku . '_clone';
}

/**
 * Implements hook_replicate_entity_ENTITY_TYPE().
 */
function commerce_replicate_replicate_entity_commerce_line_item($entity) {
  $entity->line_item_id = NULL;
  $entity->order_id = NULL;
}

/**
 * Implements hook_replicate_entity_ENTITY_TYPE().
 */
function commerce_replicate_replicate_entity_commerce_customer_profile($entity) {
  $entity->profile_id = NULL;
  $entity->revision_id = NULL;
  $entity->log = NULL;
}

/**
 * Implements hook_replicate_entity_ENTITY_TYPE().
 *
 * Commerce Order entities don't need a corresponding
 * hook_replicate_field_FIELD_TYPE() implementation since there are no field
 * types specifically for them. The default 'entityreference' field type is used
 * instead which is covered already by the replicate module.
 */
function commerce_replicate_replicate_entity_commerce_order($entity) {
  $entity->order_id = NULL;
  $entity->order_number = NULL;
  $entity->revision_id = NULL;
}

/**
 * Implements hook_replicate_field_FIELD_TYPE().
 */
function commerce_replicate_replicate_field_commerce_line_item_reference(&$entity, $entity_type, $field_name) {
  foreach ($entity->$field_name as $language => $values) {
    CommerceReplicateCommerceLineItem::cloneEntity($entity, $field_name, $language);
  }
}

/**
 * Implements hook_replicate_field_FIELD_TYPE().
 */
function commerce_replicate_replicate_field_commerce_customer_profile_reference(&$entity, $entity_type, $field_name) {
  foreach ($entity->$field_name as $language => $values) {
    CommerceReplicateCommerceCustomerProfile::cloneEntity($entity, $field_name, $language);
  }
}
